import * as fileService from "./fileService.js";

export async function getFolderFiles() {
    let result = { name: "", folders: [], files: [] };
    let folder = await fileService.selectFolderPicker();
    result.name = folder.name;
    for await (const entry of folder.values()) {
        if (entry.kind === 'directory') {
            result['folders'].push(entry.name);
        }
        else {
            result['files'].push(entry.name);
        }
    }
    return result;
}

function getTextOption() {
    let options = {
        types: [
        {
            description: 'Text Files',
            accept: {
                'text/plain': ['.txt'],
            },
        },
        ],
    };
    return options;
}

export async function openFile() {
    let options = getTextOption();
    let fileHandles = await fileService.openFilePicker(options);
    let contents = await fileService.readFileHandle_Text(fileHandles[0]);
    return { name: fileHandles[0].name, data: contents };
}

export async function saveFile(data) {
    let options = getTextOption();
    let fileHandle = await fileService.saveFilePicker(options);
    await fileService.createFile_Handle(fileHandle, data);
}

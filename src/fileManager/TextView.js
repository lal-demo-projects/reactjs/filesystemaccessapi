import React, { useState } from 'react';

function TextView(props) {
    const [fileText, setFileText] = useState("");

    React.useEffect(() => {
        setFileText(props.fileText);
    }, [props.fileText]);

    return (
        <div>
            <div className="btnBar">
                <div className="btn-group mb-2">
                    <button type="button" className="btn btn-primary" onClick={() => setFileText("")}>Clear</button>
                    <button type="button" className="btn btn-primary" onClick={() => props.handleOpenFile()}>Open</button>
                    <button type="button" className="btn btn-primary" onClick={() => props.handleSaveAsFile(fileText)}>Save as</button>
                </div>
            </div>
            <div className="form-group">
                <label>File: {props.fileName}</label>
                <textarea className="form-control" rows="20" onChange={(e) => setFileText(e.target.value)} value={fileText}></textarea>
            </div>
        </div>
    );
}

export default TextView;

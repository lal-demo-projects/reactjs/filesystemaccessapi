import React from "react";

import * as appService from '../services/appService';

import FolderView from './FolderView';
import TextView from './TextView';

class Viewport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            folderName: "",
            folderData: { folders: [], files: [] },
            fileName: "",
            fileText: ""
        };
    }
    handleSelectFolder = async () => {
        let data = await appService.getFolderFiles();
        this.setState({ folderName: data.name, folderData: data });
    };
    handleOpenFile = async () => {
        let result = await appService.openFile();
        this.setState({ fileName: result.name, fileText: result.data });
    };
    handleSaveAsFile = async (data) => {
        await appService.saveFile(data);
        this.setState({ fileName: "", fileText: "" });
    };
    render() {
        return (
            <div className="container-fluid">
                <div className="row padTop">
                    <div className="col-4">
                        <FolderView folderName={this.state.folderName} folderData={this.state.folderData} handleSelectFolder={this.handleSelectFolder} />
                    </div>
                    <div className="col-8">
                        <TextView fileName={this.state.fileName} fileText={this.state.fileText} handleOpenFile={this.handleOpenFile} handleSaveAsFile={this.handleSaveAsFile} />
                    </div>
                </div>
            </div>
        );
    }    
}

export default Viewport;

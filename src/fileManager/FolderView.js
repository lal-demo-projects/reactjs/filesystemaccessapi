
function FolderView(props)  {
    let folderItems = props.folderData.folders.map((value) =>
        <tr key={value}><td>D</td><td>{value}</td></tr>
    );
    let fileItems = props.folderData.files.map((value) =>
        <tr key={value}><td>F</td><td>{value}</td></tr>
    );
    return (
        <div>
            <div className="btnBar">
                <button className="btn btn-primary mb-2" onClick={props.handleSelectFolder}>Select Folder</button>
            </div>
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th style={{width: '5%'}}></th>
                        <th style={{width: '90%'}}>: {props.folderName}</th>
                    </tr>
                </thead>
                <tbody>
                    {folderItems}
                    {fileItems}
                </tbody>
            </table>
        </div>
    );
}

export default FolderView;

import React from "react";
import "./App.css";

import Viewport from './fileManager/Viewport';

function App() {
    return (
        <div className="App">
            <Viewport />
        </div>
    );
}

export default App;
